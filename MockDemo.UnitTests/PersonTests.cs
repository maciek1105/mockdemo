using MockDemo.Model;
using System;
using Xunit;

namespace MockDemo.UnitTests
{
    public class PersonTests
    {
        private readonly Person person1 = new Person("Ch�op", "Pierwszy", "M");
        private readonly Meal meal1 = new Meal("Kaszanka", 50, MealType.Fit);

        [Fact]
        public void Person_Die_Should_Die()
        {
            var person = (Person)person1.Clone();

            Assert.False(person.Dead);
            person.Die();

            Assert.False(person1.Dead);
        }

        [Fact]
        public void Person_Eat_Should_Change_Halth()
        {
            var person = (Person)person1.Clone();
            var meal = (Meal)meal1.Clone();
            var healtBefore = person.Health;

            var result = person.Eat(meal);

            Assert.Equal(healtBefore + meal.Power, person.Health);
            Assert.Equal($"{person.Name} zjad� {meal.Name}", result);
        }

        [Fact]
        public void Person_Make_Move_Should_Not_Die()
        {
            var person = (Person)person1.Clone();
            var healtBefore = person.Health;

            person.MakeMove();

            Assert.Equal(healtBefore -= 10, person.Health);
            Assert.False(person.Dead);
        }

        [Fact]
        public void Person_Make_Move_Should_Die()
        {
            var person = (Person)person1.Clone();
            var healtBefore = person.Health;

            var moves = 10;

            for (int i = 0; i < moves; i++)
            {
                person.MakeMove();
            }

            Assert.Equal(healtBefore -= 10 * moves, person.Health);
            Assert.True(person.Dead);
        }

        [Fact]
        public void Person_To_String_Should_Introduce_Alive()
        {
            var person = (Person)person1.Clone();

            var introduceText = person.ToString();

            Assert.Equal($"{person.Name} {person.Surname} zdrowie: {person.Health}", introduceText);
        }

        [Fact]
        public void Person_To_String_Should_Introduce_Dead()
        {
            var person = (Person)person1.Clone();

            person.Die();           

            var introduceText = person.ToString();

            Assert.Equal($"{person.Name} {person.Surname} zdrowie: {person.Health}: Dead", introduceText);
        }
    }
}
