﻿using MockDemo.Model;
using MockDemo.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace MockDemo.UnitTests
{
    public class MenuComposerServiceTests
    {
        private readonly IEnumerable<Meal> _meals = new List<Meal>() {
                new Meal("Kaszanka", 50, MealType.Fit),
                new Meal("Baleron", 45, MealType.Fit),
                new Meal("Pizza", 15, MealType.FastFood),
                new Meal ("Browarek", -4,MealType.FastFood)  };

        [Theory]
        [InlineData(MealType.FastFood)]
        [InlineData(MealType.Fit)]
        public void CreateMenu_Should_Return_Only_One_Meal_Type(MealType mealType)
        {
            var radom = new Random();
            var numberOfMealsInMenu = radom.Next(0, _meals.Count());

            var menuComposer = new MenuComposerService(_meals);

            var menu = menuComposer.CreateMenu(numberOfMealsInMenu, mealType);

            Assert.DoesNotContain(menu, m => m.MealType != mealType);
            Assert.True(menu.Count() <= numberOfMealsInMenu);
        }



    }
}
