﻿using MockDemo.Model;
using MockDemo.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MockDemo.UnitTests
{
    public class FamilyServiceTests
    {
        private readonly IEnumerable<Person> people = new List<Person>() { new Person("Chłop", "Pierwszy", "M"), new Person("Baba", "Pierwsza", "K") };

        [Fact]
        public void Should_Return_Introduction()
        {
            var familiService = new FamilyService(people);

            var introductions = familiService.GenerateIntroductions();

            Assert.Equal(people.Count(), introductions.Count());
        }
    }
}
