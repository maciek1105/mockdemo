﻿using MockDemo.Model;
using MockDemo.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MockDemo.UnitTests
{
    public class DinnerTests
    {
        private Mock<IFamilyService> _mockFamilyService = new Mock<IFamilyService>();
        private Mock<IMenuComposerService> _mockMenuCmposerService = new Mock<IMenuComposerService>();

        private readonly IEnumerable<Meal> _meals = new List<Meal>() {
                new Meal("Kaszanka", 50, MealType.Fit),
                new Meal("Baleron", 45, MealType.Fit),
                new Meal("Pizza", 15, MealType.FastFood),
                new Meal ("Browarek", -4,MealType.FastFood)  };    

        [Fact]
        public void Dinner_Should_Start()
        {
            _mockMenuCmposerService.Setup(ms => ms.CreateMenu(It.IsAny<int>(), It.IsAny<MealType>())).Returns(_meals);

            var dinner = new Dinner(_mockFamilyService.Object, _mockMenuCmposerService.Object,MealType.Fit);

            dinner.StartDinner();

            _mockFamilyService.Verify(fs => fs.GenerateIntroductions(), Times.Once);
            _mockMenuCmposerService.Verify(ms => ms.CreateMenu(It.IsAny<int>(), It.IsAny<MealType>()), Times.Once);

            _mockFamilyService.Verify(fs => fs.StartEating(It.IsAny<IEnumerable<Meal>>()), Times.AtLeastOnce);
         
        }

        [Fact]
        public void Dinner_Should_Start_Without_Eating()
        {
            _mockMenuCmposerService.Setup(ms => ms.CreateMenu(It.IsAny<int>(), It.IsAny<MealType>())).Returns(new List<Meal>());

            var dinner = new Dinner(_mockFamilyService.Object, _mockMenuCmposerService.Object, MealType.Fit);

            dinner.StartDinner();

            _mockFamilyService.Verify(fs => fs.GenerateIntroductions(), Times.Once);
            _mockMenuCmposerService.Verify(ms => ms.CreateMenu(It.IsAny<int>(), It.IsAny<MealType>()), Times.Once);

            _mockFamilyService.Verify(fs => fs.StartEating(It.IsAny<IEnumerable<Meal>>()), Times.Never);

        }
    }
}
