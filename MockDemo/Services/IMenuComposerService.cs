﻿


using MockDemo.Model;
using System.Collections.Generic;

namespace MockDemo.Services
{
    public interface IMenuComposerService
    {
        IEnumerable<Meal> Menu { get; set; }

        IEnumerable<Meal> CreateMenu(int amount, MealType type);
        IEnumerable<Meal> CreateSpicesMenu(IEnumerable<Meal> menu);
    }
}