﻿using MockDemo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MockDemo.Services
{
    public class MenuComposerService : IMenuComposerService
    {
        public MenuComposerService(IEnumerable<Meal> menu)
        {
            Menu = menu;
        }

        public IEnumerable<Meal> Menu { get; set; }

        public IEnumerable<Meal> CreateMenu(int amount, MealType type)
            => Menu.Where(m => m.MealType == type).Take(amount);

        public IEnumerable<Meal> CreateSpicesMenu(IEnumerable<Meal> menu)
        {
            var radom = new Random();

            foreach (var meal in menu)
            {
                meal.Power += radom.Next(-10, 10);
                yield return meal;
            }
        }
    }
}
