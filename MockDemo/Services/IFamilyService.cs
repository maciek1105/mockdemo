﻿using MockDemo.Model;
using System.Collections.Generic;

namespace MockDemo.Services
{
    public interface IFamilyService
    {
        int CountMembers();
        IEnumerable<string> GenerateIntroductions();
        void StartEating(IEnumerable<Meal> menu);
    }
}