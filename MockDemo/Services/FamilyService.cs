﻿using MockDemo.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("MockDemo.UnitTests")]
namespace MockDemo.Services
{
    public class FamilyService : IFamilyService
    {
        private readonly IEnumerable<Person> _familyMembers;

        public FamilyService(IEnumerable<Person> familyMembers)
        {
            _familyMembers = familyMembers;
        }

        public int CountMembers()
            => _familyMembers.Count();

        public IEnumerable<string> GenerateIntroductions()
        {
            foreach (var member in _familyMembers)
            {
                switch (member.Gender)
                {
                    case "K":
                        yield return IndroduceWoman(member);
                        break;

                    case "M":
                        yield return IndroduceMan(member);
                        break;
                }
            }
        }

        private string IndroduceMan(Person person)
        {
            var text = $"Pan {person.ToString()}";
            Console.WriteLine(text);

            return text;
        }

        private string IndroduceWoman(Person person)
        {
            var text = $"Pani {person.ToString()}";
            Console.WriteLine(text);

            return text;
        }

        public void StartEating(IEnumerable<Meal> menu)
        {
            var radom = new Random();

            foreach (var member in _familyMembers)
            {
                member.Eat(menu.ElementAt(radom.Next(0, menu.Count())));
                if (!member.Dead)
                {
                    member.MakeMove();
                }
            }
        }
    }
}
