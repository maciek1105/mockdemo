﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

[assembly: InternalsVisibleTo("MockDemo.UnitTests")]
namespace MockDemo.Model
{
    public class Person
    {
        public bool Dead { get; private set; }
        public string Name { get; private set; }
        public string Surname { get; private set; }
        public int Health { get; private set; }
        public string Gender { get; private set; }

        public Person(string name, string surname, string gender)
        {
            Name = name;
            Surname = surname;
            Health = 100;
            Dead = false;
            Gender = gender;
        }

        internal object Clone()
            => MemberwiseClone();

        internal void Die()
        {
            Dead = true;                 
        }

        public string Eat(Meal meal)
        {
            if (Dead == false)
            {
                Health += meal.Power;
            }

            var text = $"{Name} zjadł {meal.Name}";

            Console.WriteLine(text);
            return text;
        }

        public void MakeMove()
        {
            if (Dead == false)
            {
                Health -= 10;

                if (Health <= 0)
                {
                    Die();
                }
            }
        }

        public override string ToString()
        {
            var prefix = Dead ? ": Dead" : "";

            return $"{Name} {Surname} zdrowie: {Health}{prefix}";
        }
    }
}
