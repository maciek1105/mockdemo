﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("MockDemo.UnitTests")]
namespace MockDemo.Model
{
    public class Meal
    {  
        public Meal(string name, int power, MealType mealType)
        {
            Name = name;
            Power = power;
            MealType = mealType;
        }

        public string Name { get; set; }
        public int Power { get; set; }
        public MealType MealType { get; set; }

        internal object Clone()
            => MemberwiseClone();       
    }

    public enum MealType
    {
        Fit,
        FastFood
    }
}