﻿using MockDemo.Services;
using System;
using System.Linq;

namespace MockDemo.Model
{
    public class Dinner
    {
        private readonly IFamilyService _familiService;
        private readonly IMenuComposerService _menuService;
        private readonly MealType _mealType;

        public Dinner(IFamilyService familiService, IMenuComposerService menuService, MealType type)
        {
            _familiService = familiService;
            _menuService = menuService;
            _mealType = type;
        }

        public void StartDinner()
        {
            foreach(var introduction in _familiService.GenerateIntroductions())
            {
                Console.WriteLine(introduction);
            }

            var menu = _menuService.CreateMenu(_familiService.CountMembers(), _mealType);

            if (menu.Count() > 0)
            {
                _familiService.StartEating(menu);
            }
        }       
        
    }
}
