﻿using MockDemo.Model;
using MockDemo.Services;
using System;
using System.Collections.Generic;

namespace MockDemo
{
    class Program
    {
        static void Main(string[] args)
        {            
            var people = new List<Person>() { new Person("Chłop", "Pierwszy", "M"), new Person("Baba", "Pierwsza", "K") };
            var meals = new List<Meal>() {
                new Meal("Kaszanka", 50, MealType.Fit), 
                new Meal("Baleron", 45, MealType.Fit), 
                new Meal("Pizza", 15, MealType.FastFood),
                new Meal ("Browarek", -4,MealType.FastFood)  };

            var familiService = new FamilyService(people);
            var menuService = new MenuComposerService(meals);

            var dinner = new Dinner(familiService, menuService, MealType.FastFood);

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine($"Obiad numer {i}");
                dinner.StartDinner();
            }

        }
    }
}
